<?php
// src/AppBundle/Controller/HomePageController.php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Contact;
use AppBundle\Form\ContactType;

class HomePageController extends Controller {
	
	/**
	 * @Route("/", name="homepage")
	 */
	public function homepageAction() {		
		return $this->render('AppBundle:homepage:homepage.html.twig');		 
	}
	
	/**
	 * @Route("/about", name="about")
	 */
	public function aboutAction() {
		return $this->render('AppBundle:about:about.html.twig');
	}
	
	/**
	 * @Route("/contact", name="contact")
	 */
    public function contactAction(Request $request)
    {
        $enquiry = new Contact();
        $form = $this->createForm(ContactType::class, $enquiry);
 
         $this->request = $request;
			if ($request->getMethod() == 'POST') {
			$form->bind($request);
 
 
            if ($form->isValid()) {
                // Perform some action, such as sending an email
				$message = \Swift_Message::newInstance()
					->setSubject('Contact enquiry from HubShark')
					->setFrom('info@hubshark.com')
					->setTo($this->container->getParameter('app.emails.contact_email'))
					->setBody($this->renderView('AppBundle:Page:contactEmail.txt.twig', array('enquiry' => $enquiry)));
				$this->get('mailer')->send($message);
			
			$this->get('session')->getFlashbag('blog-notice', 'Your contact enquiry was successfully sent. Thank you!');
                 
                // Redirect - This is important to prevent users re-posting
                // the form if they refresh the page
                return $this->redirect($this->generateUrl('contact'));
            }
        }
 
        return $this->render('AppBundle:contact:contact.html.twig', array(
            'form' => $form->createView()
        ));
    }

	
	/**
	 * @Route("/search", name="search")
	 */
	public function searchAction() {
		return $this->render('AppBundle:search:search.html.twig');
	}

	/**
	 * @Route("/sidebar", name="sidebar")
	 */
	public function sidebarAction() {
		return $this->render('AppBundle:sidebar:sidebar.html.twig');
	}

}
